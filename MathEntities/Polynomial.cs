﻿using System;
using System.Text;

namespace MathEntities
{
    public class Polynomial
    {
        private double[] _coefficients;

        /// <summary>
        /// Creates an instance of Polynomial from coefficient array.
        /// Array index is equals to a power of argument.
        /// </summary>
        /// <exception cref="ArgumentNullException"></exception>
        /// <param name="coefficientsArray"></param>
        public Polynomial(double[] coefficientsArray)
        {
            if (coefficientsArray == null)
            {
                throw new ArgumentNullException("Coefficient array cannot be null");
            }
            _coefficients = coefficientsArray;
        }
        /// <summary>
        /// Creates an instance of Polynomial from coefficient array.
        /// Array index is equals to a power of argument.
        /// </summary>
        /// <exception cref="ArgumentNullException"></exception>
        /// <param name="coefficientsArray"></param>
        public Polynomial(int[] coefficientsArray)
        {
            if (coefficientsArray == null)
            {
                throw new ArgumentNullException("Coefficient array cannot be null");
            }
            _coefficients = new double[coefficientsArray.Length];
            for (int i = 0; i < _coefficients.Length; i++)
            {
                _coefficients[i] = coefficientsArray[i];
            }
        }
        public double[] CoefficientsArray
        {
            get { return _coefficients; } 
        }
        public int MaxPow
        {
            get { return _coefficients.Length - 1; }
        }
        /// <summary>
        /// Gets Polynomial value.
        /// </summary>
        /// <param name="argument">Polynomial argument</param>
        /// <returns></returns>
        public double GetValue(double argument)
        {
            double result = 0;
            for (int i = 0; i < _coefficients.Length; i++)
            {
                result += _coefficients[i] * Math.Pow(argument, i);
            }
            return result;
        }
        private Polynomial DoArithmetic(Polynomial polynom1, Polynomial polynom2, bool isAdd)
        {
            if (polynom1 == null || polynom2 == null)
            {
                throw new ArgumentNullException("Arguments cannot be null");
            }
            double[] first = polynom1.CoefficientsArray;
            double[] second = polynom2.CoefficientsArray;
            double[] coeffs;
            if (polynom1.MaxPow > polynom2.MaxPow)
            {
                coeffs = new double[polynom1.MaxPow + 1];
            }
            else
            {
                coeffs = new double[polynom2.MaxPow + 1];
            }
            if (isAdd)
            {
                for (int i = 0; i < first.Length; i++)
                {
                    coeffs[i] += first[i];
                }
                for (int i = 0; i < second.Length; i++)
                {
                    coeffs[i] += second[i];
                }
            }
            else
            {
                for (int i = 0; i < first.Length; i++)
                {
                    coeffs[i] += first[i];
                }
                for (int i = 0; i < second.Length; i++)
                {
                    coeffs[i] -= second[i];
                }
            }
            
            return new Polynomial(coeffs);
        }
        /// <summary>
        /// Adds two Polynomials.
        /// </summary>
        /// <exception cref="ArgumentNullException"></exception>
        /// <param name="polynom1"></param>
        /// <param name="polynom2"></param>
        /// <returns></returns>
        public Polynomial Add(Polynomial polynom1, Polynomial polynom2)
        {
            return this.DoArithmetic(polynom1, polynom2, true);
        }
        /// <summary>
        /// Substracts two Polynomials.
        /// </summary>
        /// <exception cref="ArgumentNullException"></exception>
        /// <param name="polynom1"></param>
        /// <param name="polynom2"></param>
        /// <returns></returns>
        public Polynomial Substract(Polynomial polynom1, Polynomial polynom2)
        {
            return this.DoArithmetic(polynom1, polynom2, false);
        }
        /// <summary>
        /// Multiplies two Polynomials.
        /// </summary>
        /// <exception cref="ArgumentNullException"></exception>
        /// <param name="polynom1"></param>
        /// <param name="polynom2"></param>
        /// <returns></returns>
        public Polynomial Multiply(Polynomial polynom1, Polynomial polynom2)
        {
            int length = polynom1.MaxPow + polynom2.MaxPow + 1;
            double[] coeffs = new double[length];
            double[] aCoeffs = polynom1.CoefficientsArray;
            double[] bCoeffs = polynom2.CoefficientsArray;
            for (int i = 0; i < coeffs.Length; i++)
            {
                double sum = 0;
                for (int j = 0; j < aCoeffs.Length; j++)
                {
                    for (int k = 0; k < bCoeffs.Length; k++)
                    {
                        if (j + k == i)
                        {
                            sum += aCoeffs[j] * bCoeffs[k];
                        }
                    }
                }
                coeffs[i] = sum;
            }
            return new Polynomial(coeffs);
        }
        public override string ToString()
        {
            StringBuilder result = new StringBuilder((this.MaxPow + 1) * 4);
            for (int i = 0; i < this._coefficients.Length; i++)
            {
                if (_coefficients[i] > 0)
                {
                    result.Append($"+{_coefficients[i]}*X^{i}");
                }
                else
                {
                    result.Append($"{_coefficients[i]}*X^{i}");
                }
            }
            return result.ToString();
        }
    }
}
